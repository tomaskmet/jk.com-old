
var contact = angular.module('jkApp.contact', [
  'ui.router',
  'jkApp',
  // 'ngSanitize'
]);

contact.controller('contactCtrl', ['$scope', '$http' , '$timeout' , contactCtrl]);

//**********************************
// contactCtrl controller
//***********************************

function contactCtrl($scope, $http, $timeout) {
  var vm = this;

  vm.contact = {} ;
  vm.postData = {};
  vm.messageSend = false;
  vm.sendProgress = false;
  vm.formErrors = { name: null, email: null, message: null, reqBody: null };

  var baseUrl =  window.location.hostname == 'jagodakondratiuk.com' ? 'http://' + window.location.hostname + '/api/contact' : 'http://' + window.location.hostname + ':8080/api/contact' ;

  var emptyFields = [];
  var validEmail = false;

  var validateFormData = function(data){
    var validFormData = false;
    var validateFields = [ 'name', 'email', 'message' ];
    emptyFields = [];
    for (var i = 0; i < validateFields.length; i++) {
      if( !data.hasOwnProperty(validateFields[i]) || data[validateFields[i]] == " " ) {
        emptyFields.push(validateFields[i]);
        vm.formErrors[validateFields[i]] = 'empty ' + validateFields[i] + ' field';
      }
    }
    if(data.hasOwnProperty('email') && data.email != undefined && data['email'].includes("@") && data['email'].includes(".") ) {
      validEmail = true
    }else{
      vm.formErrors.email = 'invalid email';
    };
    if(emptyFields.length == 0 && validEmail ) validFormData = true;
    return validFormData ;
  }

  vm.submitFormAction = function ( contact ) {

    vm.formErrors = { name: null, email: null, message: null, reqBody: null};

    if( validateFormData( contact ) ){
      vm.sendProgress = true;
      vm.postData = angular.copy( contact );
      $http({
        method: 'POST',
        url: baseUrl,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        transformRequest: function(obj) {
          var str = [];
          for(var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: vm.postData
      }).then(function successCallback(response, status) {
        // console.log('success FRONT -> ', response, status);
        $timeout(function(){
          vm.messageSend = true;
        },1000);

      }, function errorCallback(response, status) {
        // console.log('error FRONT -> ', response,  'asdasd ---  Status' + status);
        vm.formErrors.reqBody = response.data;
        vm.messageSend = false;
        vm.sendProgress = false;
      });
    }

  };

};
