var jkApp = angular.module('jkApp', [
  'ui.router',
  'ngAnimate',
  'jkApp.projects',
  'jkApp.projectDetail',
  'jkApp.contact',
]);


jkApp.config(function($stateProvider){
  $stateProvider
  .state('hello', {
    url: "/",
    views: {
      'main': {
        templateUrl: 'app/hello.tmpl.html',
        controller: 'helloCtrl as vm'
      }
    }
  })
  .state('analog', {
    url: "/analog",
    views: {
      'main': {
        templateUrl: 'app/projects/projects.tmpl.html',
        controller: 'projectsCtrl as vm'
      },
    }
  })
  .state('digital', {
    url: "/digital",
    views: {
      'main': {
        templateUrl: 'app/projects/projects.tmpl.html',
        controller: 'projectsCtrl as vm'
      },
    }
  })
  .state('project', {
    url: "/project/:project",
    views: {
      'main': {
        templateUrl: 'app/projects/projectdetail/projectdetail.tmpl.html',
        controller: 'projectDetailCtrl as vm'
      }
    }
  })
  .state('about', {
    url: "/about",
    views: {
      'main': {
        templateUrl: 'app/about/about.tmpl.html',
      },
    }
  })
  .state('contact', {
    url: "/contact",
    views: {
      'main': {
        templateUrl: 'app/contact/contact.tmpl.html',
        controller: 'contactCtrl as vm'

      },
    }
  })
});

jkApp.controller('mainCtrl', ['$scope', '$state', '$rootScope', '$window', '$document', '$timeout', 'projectsData', mainCtrl]);
jkApp.controller('helloCtrl', ['$scope', '$state',  '$rootScope', helloCtrl]);
jkApp.service('projectsData',['$http' , projectsData]);



//**********************************
// Projects Data Service
//***********************************

function projectsData($http) {
  var model = this;
  var URLS = {
    FETCH: 'data/gallery-projects.json'
  };
  var projects;
  model.getProjectsData = function() {
    return $http.get(URLS.FETCH);
  };
};

//**********************************
// MAIN controller
//***********************************

function mainCtrl($scope, $state, $rootScope,$window, $document,  $timeout, projectsData, projectsMenuCtrl) {
  var vm = this;
  vm.menu = "app/menu/menu.tmpl.html";

if($window.location.hash == "") $state.go('hello');

  projectsData.getProjectsData().then(function(result){
    $scope.projects = result.data;
  });

  vm.stateChangeClasses  = { project:'left-right', hello: 'top-bottom' , digital: 'top-bottom', analog: 'bottom-top', about: 'left-right', contact: 'right-left' };
  vm.changeClass = null;
  vm.overlayAniTime = 500;

  var $menu;
  var $menuLinks;


  vm.changeClassUpdate = function(toState){ // changeState
    // var classNew = null;
    var fromState = $state.current.name;
    if(vm.stateChangeClasses[toState]){
      if(fromState == 'project.detail' && toState == 'project.detail') return;
      if(toState == 'project.detail' && fromState == 'analog' ){
        vm.changeClass = vm.stateChangeClasses['analog'];
        classNew = vm.stateChangeClasses['analog'];
      }else if(toState == 'project.detail' && fromState == 'digital'){
        vm.changeClass = vm.stateChangeClasses['digital'];
        classNew = vm.stateChangeClasses['digital'];
      }else{
        vm.changeClass = vm.stateChangeClasses[toState];
        classNew = vm.stateChangeClasses[toState];
      }
      // return classNew;
    }
  };

  vm.menuStateChange = function(stateTo, project){
    if($state.current.name == stateTo) return;
    vm.changeClassUpdate(stateTo);
    $menu.classList.add(vm.changeClass);
    $menu.classList.add('active');
    $timeout(function(){
      if(project == null){
        $state.go(stateTo, {});
      }else {
        $state.go(stateTo, { 'project' : project});
      }
    },vm.overlayAniTime); // overlay animation in time
  }

  vm.stateChangeLoaded = function(){
    $menu = $document[0].getElementById('menu');
    if(!$menu) return;
    $menuLinks = $menu.getElementsByClassName('menu-link');

    //menu active class START
    for (var i = 0; i < $menuLinks.length; i++) {
      $menuLinks[i].classList.remove('active');
    }
    activeMenuTab = $menu.getElementsByClassName('link-' + $state.current.name);
    if(activeMenuTab.length > 0) activeMenuTab[0].children[0].classList.add('active');
    var $helloLink = $menu.getElementsByClassName('link-hello')[0];
    //menu active class END

    //hello menu link hide in HELLO state
    if($state.current.name == 'hello'){
      $helloLink.classList.add('hide');
    } else{
      $helloLink.classList.remove('hide');
    }

    $timeout(function(){
      if(vm.changeClass != null){
        $menu.classList.remove('active');
      }
    }, 500 ); // hide black overlay after loaded and animated
    $timeout(function(){
      if(vm.changeClass != null){
        $menu.classList.remove($scope.vm.changeClass);
      }
    },1000);//animation lenght -> 1000
  }


  $rootScope.$on('$viewContentLoaded', function(event) {
    vm.stateChangeLoaded();
  });


  vm.$onInit = function(){
    var $loader = document.getElementById('loaderMain');
    $loader.classList.add('hide');
    setTimeout(function () {
      $loader.style.display = 'none';
    }, 2500); //1000 -> loader hide time

  }



};

//**********************************
// Hello controller
//***********************************
function helloCtrl($scope, $state, $rootScope ) {
  var vm = this;
  vm.helloMoveInDelay = 1500;
  vm.helloWriteDelay = vm.helloMoveInDelay + 1000;
};


//************************************************
// Directives -> scroll Trigger MAIN
//************************************************

var helloSecLoad = false;
jkApp.directive('helloMoveIn', function($timeout){
  return{
    link: function(scope, element, attrs){
      if(helloSecLoad){
        element[0].classList.add('move-in-fast');
        return;
      }
      $timeout(function(){
        element[0].classList.add('move-in');
        helloSecLoad = true;
      }, scope.vm.helloMoveInDelay);
    }
  }
});


jkApp.directive('imgLoader', function($timeout){
  return{
    link: function(scope, element, attrs){
      element[0].classList.add('loading');
      var loader = document.createElement("span");
      loader.classList.add('loader');
      // element[0].parentNode.appendChild(loader);

      element.bind("load",function(e){
        // element[0].parentNode.removeChild(loader);
        element[0].classList.remove('loading');
      });
    }
  }
});

jkApp.directive('disableRighClick', function($timeout){
  return{
    link: function(scope, element, attrs){
      element.bind("contextmenu",function(e){
        // e.preventDefault();
      });
    }
  }
});


jkApp.directive('randomMove', function($timeout, $window){
  return{
    // restrict: 'A',
    scope: {
      timeinterval: '=',
      rndtimemax: '=',
      movemax: '='
    },
    link: function(scope, element, attrs){

      // DEFAULT SETTINGS moveMax = 50; rndTimeMax = 8000; timeInterval = 1500;

      var moveMax = scope.movemax;
      if($window.innerWidth < 600) moveMax = moveMax / 3 ;
      var rndTimeMax = scope.rndtimeMax;
      var timeInterval = scope.timeinterval;

      var randomMove = Math.floor(Math.random() * moveMax*2) - moveMax ;
      var randomTime = Math.floor(Math.random() * rndTimeMax);
      var elOrigStyle = element[0].style.left;

      setInterval(function(){
        // random move, 50% chance every $timeInterval
        randomBoolean = Math.round(Math.random()) ;
        if(randomBoolean == 1){
          element[0].style.left = elOrigStyle + (randomMove + 'px');
          setTimeout(function() {
            element[0].style.left = elOrigStyle ;
          },50)
        }
        // random move according to random timeout  $randomTime
        setTimeout(function(){
          randomMove = Math.floor(Math.random() * moveMax*2) - moveMax ;
          element[0].style.left = elOrigStyle + (randomMove + 'px');
          setTimeout(function(){
            element[0].style.left = elOrigStyle;
          },50)
        },randomTime);
      },timeInterval);
    }
  }
});


jkApp.firstHelloStateWas = false;

jkApp.directive('stringWrite', function($timeout, $rootScope){
  return{
    link: function(scope, element, attrs){

      if(jkApp.firstHelloStateWas == true ) return;
      jkApp.firstHelloStateWas = true;

      var stringToAnimate = element[0].innerText;
      var arrayFromString = stringToAnimate.split('');
      var arrayAnimated = [];
      var aniSpeed = 35 ;
      element[0].innerHTML = "<span style='opacity:0 '>.</span>";
      var i = 0;
      var animateStringInt;

      function stopInt() { clearInterval(animateStringInt) }
      function addString() {
        arrayAnimated.push(arrayFromString[i]);
        element[0].innerHTML = arrayAnimated.join('');
        i++
        if( arrayFromString.length == arrayAnimated.length){
          stopInt();
          element[0].classList.add('in-out-ani');
          setTimeout(function(){
            element[0].classList.remove('active');
            element[0].classList.remove('in-out-ani');
          },2500)
        }
      }

      $rootScope.$on('$viewContentLoaded', function(event,animateStringInt) {
        setTimeout(function(){
          element[0].classList.add('active');
          animateStringInt = setInterval(function(){ addString() }, aniSpeed);
        },scope.vm.helloWriteDelay);
      });


    }
  }
})
