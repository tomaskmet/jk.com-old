var projectDetail = angular.module('jkApp.projectDetail', [
  'ui.router',
  'ngAnimate',
  'jkApp',
  'jkApp.projects'
]);

jkApp.config(function($stateProvider){
  $stateProvider
  .state('project.detail', {
    url: "/:picture",
    views: {
      'galleryImage': {
        templateUrl: 'app/projects/projectdetail/galleryimage.tmpl.html',
      }
    }
  })

});

projectDetail.controller('projectDetailCtrl', ['$scope', '$state','$stateParams', '$filter', '$rootScope', '$timeout', '$window'  , projectDetailCtrl]);

function projectDetailCtrl($scope, $state, $stateParams, $filter, $rootScope, $timeout, $window){
  var vm = this;

  vm.loadMoreLimit = 9;
  vm.prevBtn = false;
  vm.nextBtn = false;
  vm.galleryShow = { start: 0  , end: vm.loadMoreLimit };
  vm.galleryImgMainState = null;
  // vm.galleryImgMainLoading = false;
  vm.smallGalleryHide = false;

  let galleryImgAniTime = 300;

  vm.galleryShowChange = function(direction){
    if(direction == 'next'){
      vm.galleryShow.start += vm.loadMoreLimit;
      vm.galleryShow.end += vm.loadMoreLimit;
      vm.galleryShow.end >= vm.projectData.gallery.length ?  vm.nextBtn = false :  vm.nextBtn = true;
      vm.prevBtn = true;
    }else{
      vm.galleryShow.start -= vm.loadMoreLimit;
      vm.galleryShow.end -= vm.loadMoreLimit;
      if(vm.galleryShow.start == 0)
      vm.prevBtn = false;
      if(vm.galleryShow.end <= vm.projectData.gallery.length)
      vm.nextBtn = true;
    }
  }

  vm.galleryImgNextPrev = function(direction){
    // vm.galleryImgMainLoading = true;
    vm.galleryImgMainState = 'out loading';
    $timeout(function(){
      $state.go( 'project.detail' , {picture: vm.galleryPictureCurrent + direction } );
    },galleryImgAniTime);
  }

  vm.galleryImgChangeTo = function(i){
    if($state.params.picture == i) return;
    // vm.galleryImgMainLoading = true;
    vm.galleryImgMainState = 'out loading';
    $timeout(function(){
      $state.go( 'project.detail' , { picture: i + vm.galleryShow.start } );
      $window.innerWidth <= 768 ?  vm.smallGalleryHide = true  : vm.smallGalleryHide = false;
    },galleryImgAniTime);
  }


  vm.projectData;
  var projects = $scope.projects;

  $scope.$on('$viewContentLoaded', function(event) {
    vm.projectData =  $filter('filter')(projects, $stateParams.project)[0];
    if($state.current.name == 'project' && $state.params.picture == undefined){
      $state.go('project.detail', { 'picture': '0' } );
    }

    vm.galleryPictureCurrent = Number($state.params.picture);

    if(vm.galleryPictureCurrent >= vm.projectData.gallery.length){
      vm.galleryPictureCurrent = 0;
      $state.go( $state.current.name , {project : $state.params.project, picture: vm.galleryPictureCurrent } );
    }else if(vm.galleryPictureCurrent <= -1){
      vm.galleryPictureCurrent = vm.projectData.gallery.length -1;
      $state.go( $state.current.name , {project : $state.params.project, picture: vm.galleryPictureCurrent } );
    }

    var interval = Math.floor(vm.galleryPictureCurrent / vm.loadMoreLimit) * vm.loadMoreLimit
    vm.galleryShow = { start: interval  , end:  interval + vm.loadMoreLimit };

    vm.galleryShow.start < vm.loadMoreLimit ?  vm.prevBtn = false : vm.prevBtn = true;
    vm.galleryShow.end >= vm.projectData.gallery.length ?  vm.nextBtn = false :  vm.nextBtn = true;

  });

};


//***********************************************************************
//-> DIRECTIVES
//************************************************************************

projectDetail.directive('galImgLoader', function($timeout){
  return{
    link: function(scope, element, attrs){
      element.bind("load",function(){
        scope.vm.galleryImgMainState = 'loading' ;
        $timeout(function(){
          scope.vm.galleryImgMainState = 'in' ;
        },150);
      });
    }
  }
});

projectDetail.directive('swipeGalImg', function($timeout){
  return{
    link: function(scope, element, attrs){

        var $galleryImgSlider = element[0];
        var hammer    = new Hammer.Manager($galleryImgSlider);
        var swipe     = new Hammer.Swipe();
        hammer.add(swipe);
        hammer.on('swipeleft', function(){
          scope.vm.galleryImgNextPrev(1);
        });
        hammer.on('swiperight', function(){
          scope.vm.galleryImgNextPrev(-1);
        });


    }
  }
});
