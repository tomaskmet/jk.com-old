var projects = angular.module('jkApp.projects', [
  'ui.router',
  'ngAnimate',
  'jkApp',
  'jkApp.projectDetail'
]);

projects.controller( 'projectsCtrl' , [ '$scope' ,'$state', '$stateParams', '$filter', '$rootScope','$timeout', '$window',  projectsCtrl ]);

function projectsCtrl( $scope, $state , $stateParams , $filter, $rootScope, $timeout, $window) {
  var vm = this;

  vm.stateChangeTo = function(stateTo, project){
    $scope.$parent.vm.menuStateChange(stateTo, project);
  }

  vm.projects = $scope.projects;
  vm.projectsFiltered;
  vm.prevBtn = false;
  vm.nextBtn = false;

  //thumbs Size and ThumbWrapper size
  var maxThumbWidth = 300;
  var minThumbWidth = 200;
  var maxThumbHeight = 225;
  var rowsNum = null;
  var colsNum = null;
  var screenVertical = null ;
  var thumbWNew;
  var thumbHNew;
  vm.projectsWidth = null ;
  vm.projectsHeight = null ;
  vm.loadMoreLimit = null;
  vm.thumbWidth ;
  vm.thumbHeight ;

  thumbsMoveRight = function(thumbW){
    if($window.innerWidth < 600)
    return 0;
    if($window.innerWidth < 769 && screenVertical == true)
    return thumbW / 4;

    return thumbW / 2;

  }

  vm.thumbsSizeSet = function( w , h ){
      var thumbHAdjust = 5;
      var thumbW_HCoef = 0.75;
      rowsNum = 3;
      colsNum = 3;
      vm.projectsWidth = w ;
      vm.projectsHeight = h ;
      vm.thumbMargin = 15  ; // px
      screenVertical = vm.projectsWidth < vm.projectsHeight ? true : false ;
      if($window.innerWidth < 1450){
        vm.thumbMargin = 10  ; // px
      }
      if($window.innerWidth < 769 && screenVertical == true){
        colsNum = 2;
      }
      if($window.innerWidth < 600){
        vm.thumbMargin = 7  ; // px
        colsNum = 1;
        thumbHAdjust = 5;
        thumbW_HCoef = 0.65;
      }
      vm.loadMoreLimit = rowsNum * colsNum;


      thumbHNew = (( vm.projectsHeight - ( 6 * vm.thumbMargin ) ) / rowsNum ) - thumbHAdjust ; //5
      if(thumbHNew > maxThumbHeight){ thumbHNew = maxThumbHeight; }
      thumbWNew =  thumbHNew / thumbW_HCoef;
      vm.thumbsWrapper = ( thumbWNew * colsNum ) + ( 6 * vm.thumbMargin ) + 10 + thumbsMoveRight(thumbWNew) ; //px

      if(vm.thumbsWrapper > vm.projectsWidth){
        vm.thumbsMoveRight = thumbWNew/2;
        thumbWNew = (( vm.projectsWidth - ( 6 * vm.thumbMargin )  - thumbsMoveRight(thumbWNew) ) / colsNum ) - thumbHAdjust;
        if(thumbWNew > maxThumbWidth){ thumbWNew = maxThumbWidth; }
        thumbHNew = thumbWNew * thumbW_HCoef;
        vm.thumbsWrapper = ( thumbWNew * colsNum ) + ( 6 * vm.thumbMargin ) + 10  ; //px
      }

      vm.thumbHeight = thumbHNew + 'px'; //px
      vm.thumbWidth = thumbWNew + 'px';  //px
      vm.thubsWrapperRight = thumbsMoveRight(thumbWNew) / 2 + 'px';
      vm.projectsShow = { start: 0 , end: vm.loadMoreLimit };

  }

  vm.thumbMove = function(index){
    var moveRight = 0;
    if(colsNum == 1) {
      moveRight =  0;
    }else {
      if( index - colsNum >= 0 && index - colsNum < colsNum){
        moveRight = 'calc('+ thumbsMoveRight(thumbWNew)  + 'px )';
      }else{
        moveRight = 0;
      }
    }
    return moveRight;
  }

  vm.projectsShowNext = function(direction){
    if(direction == 'next'){
      vm.projectsShow.start += vm.loadMoreLimit;
      vm.projectsShow.end += vm.loadMoreLimit;
      vm.projectsShow.end >= vm.projectsFiltered.length ?  vm.nextBtn = false :  vm.nextBtn = true;
      vm.prevBtn = true;
    }else{
      vm.projectsShow.start -= vm.loadMoreLimit;
      vm.projectsShow.end -= vm.loadMoreLimit;
      if(vm.projectsShow.start == 0)
      vm.prevBtn = false;
      if(vm.projectsShow.end <= vm.projectsFiltered.length)
      vm.nextBtn = true;
    }
  }

  $scope.$on('$viewContentLoaded', function(event) {
    $timeout(function () {
      vm.projectsFiltered = $filter('filter')(vm.projects, $state.current.name);
      vm.projectsShow = { start: 0 , end: vm.loadMoreLimit };

      if(vm.projectsFiltered.length > vm.loadMoreLimit)
      vm.nextBtn = true;

    }, 0);
  });

  // $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
  // });

};


//***********************************************************************
//-> DIRECTIVES
//************************************************************************


projects.directive("projectsWidth", function($window, $document, $timeout){
  return{
    link: function(scope, element, attr){

      function onResize(){
        // scope.vm.projectsWidth = element[0].clientWidth;
        // scope.vm.projectsHeight = element[0].clientHeight;

        $timeout(function(){

          scope.vm.thumbsSizeSet(element[0].clientWidth, element[0].clientHeight);

        }, 0 );

        // scope.$digest();

      };

      onResize();

      angular.element($window).on('resize', onResize);

      scope.$on('$destroy',   function () {
        angular.element($window).off('resize', onResize);
      });

    }
  }
})
