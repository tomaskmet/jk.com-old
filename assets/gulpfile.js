/*
  Dostepne taski:
  gulp - watch na sass, concat/uglify js
*/
var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass'); // Sass engine
var autoprefixer = require('gulp-autoprefixer'); // prefixy do przegladarek
var rename = require('gulp-rename'); // zmiana nazwy pliku
var uglify = require('gulp-uglify'); // uglify do JS
var htmlmin = require('gulp-htmlmin'); // kompresja HTML
var concat = require('gulp-concat'); // laczenie plikow
var replace = require('gulp-replace'); // regexy i usuwanie stringow
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel'); // bebel, es6



// https://github.com/terinjokes/gulp-uglify
var ulgifyConfig = {
  sourceMap: false,
  preserveComments: false,
  mangle: false,
  beautify: false,
  reserveDOMProperties: true,
  mangleProperties: false,
  screwIE8: true
};

// https://github.com/dlmanning/gulp-sass
var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

// ktore przegladarki obslugujemy CSSem, bazowane na danych z
// http://caniuse.com/
var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 2%', 'Firefox ESR']
};

// Kompilacja Sass, przekazujemy jej {sassOptions}
gulp.task('sass', function() {
    return gulp.src('./scss/*.scss')
      .pipe(sass(sassOptions).on('error', sass.logError))
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe(gulp.dest('./css'));
});

// laczy i uglifikuje pliki js

gulp.task('es6', () => {
	return gulp.src('./js/es6/main.es6.js')
		.pipe(sourcemaps.init())
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./js/es6'));
});

// obserwowanie zmian na Sass

gulp.task('watch', () => {
    gulp.watch('./scss/*.scss', ['sass'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
    gulp.watch(['./js/es6/main.es6.js'], ['es6'])
    .on('change', (event) => {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('default', ['watch', 'sass', 'concat_scripts']);
gulp.task('compile', ['sass', 'concat_scripts']);
