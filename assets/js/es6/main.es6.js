// "use strict";

(function (jk, undefined) {

  jk.init =  () => {
    jk.mainLoader();
  };


  jk.mainLoader = () => {
    var $loader = document.getElementById('loaderMain');
    setTimeout( () => {
      // var $menu = document.getElementById('menu');
      // $menu.classList.add('active');

      $loader.classList.add('hide');
    },1500); //1000

    setTimeout( () => {
      $loader.style.display = 'none';
    },1800); //1500
  };

  window.onload = () => {
    jk.init();
  };
})(window.jk = window.jk || {});
//# sourceMappingURL=main.js.map
